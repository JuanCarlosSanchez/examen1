defmodule Exam1 do

  # ============================
  # PARTE 1
  # ============================
  def funny_sort(str) do
    lista1 = Enum.filter(str, fn letra -> Regex.match?(~r/^[\D]+$/, letra) end)
    lista2 = Enum.filter(str, fn num -> Regex.match?(~r/[0-9]/, num) end) |> Enum.sort
    lista1 ++ lista2
  end

  # ============================
  # PARTE 2
  # ============================
  def count(_seq, _char) do
  end
  def histogram(_seq) do
  end
end
