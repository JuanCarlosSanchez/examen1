defmodule Part1Test do
  use ExUnit.Case

  #@tag :skip
  test "should return ['10', '11', '12'] when called with ['10', '11', '12']" do
    input = ~w{10 11 12}
    assert Exam1.funny_sort(input) == input
  end

  #@tag :skip
  test "should return a ['10', '11', '12'] when called with ['10', '12', '11']" do
    input = ~w{10 12 11}
    assert Exam1.funny_sort(input) == Enum.sort(input)
  end

  #@tag :skip
  test "should return a ['z', 'a', '10', '11', '12'] when called with ['10', '12', '11', 'z', 'a']" do
    letters = ~w{z a}
    numbers = ~w{10 12 11}
    {first, second} = Enum.split(Exam1.funny_sort(numbers ++ letters), 2)
    assert MapSet.new(first) == MapSet.new(letters) # Order does not matter
    assert second == Enum.sort(numbers)
  end

  #@tag :skip
  test "should return a ['z', 'a'] in any order, followed by ['-10a', '11x2', '12'] when called with ['z', '12', '11x2', '-10a', 'a']" do
    letters = ~w{z a}
    numbers = ~w{12 11x2 -10a}
    {first, second} = Exam1.funny_sort( ["z" | numbers] ++ ["a"]) |> Enum.split(2)
    assert MapSet.new(first) == MapSet.new(letters) # Order does not matter
    assert second == Enum.reverse(numbers)
  end
end
