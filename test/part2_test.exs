defmodule Part2Test do
  use ExUnit.Case

  @tag :skip
  test "empty dna string has no adenine" do
    assert Exam1.count('', ?A) == 0
  end

  @tag :skip
  test "repetitive cytosine gets counted" do
    assert Exam1.count('CCCCC', ?C) == 5
  end

  @tag :skip
  test "counts only thymine" do
    assert Exam1.count('GGGGGTAACCCGG', ?T) == 1
  end

  @tag :skip
  test "empty dna string has no nucleotides" do
    expected = %{?A => 0, ?T => 0, ?C => 0, ?G => 0}
    assert Exam1.histogram('') == expected
  end

  @tag :skip
  test "repetitive sequence has only guanine" do
    expected = %{?A => 0, ?T => 0, ?C => 0, ?G => 8}
    assert Exam1.histogram('GGGGGGGG') == expected
  end

  @tag :skip
  test "counts all nucleotides" do
    s = 'AGCTTTTCATTCTGACTGCAACGGGCAATATGTCTCTGTGTGGATTAAAAAAAGAGTGTCTGATAGCAGC'
    expected = %{?A => 20, ?T => 21, ?C => 12, ?G => 17}
    assert Exam1.histogram(s) == expected
  end
end
